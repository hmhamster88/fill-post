document.addEventListener('DOMContentLoaded', () => {

  const inputs = $('input[type="text"]');

  $('#save').on('click', () => {
    const toSave = {};
    for(const input of inputs) {
      const jInput = $(input);
      const id = jInput.attr('id');
      const val = jInput.val();
      toSave[id] = val;
    }

    chrome.storage.sync.set(toSave);

  });

  chrome.storage.sync.get((items) => {
    for(const input of inputs) {
      const jInput = $(input);
      const id = jInput.attr('id');
      jInput.val(items[id]);
    }
  });

});
