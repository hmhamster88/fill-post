document.addEventListener('DOMContentLoaded', () => {
  var button = document.getElementById('fill-button');
  button.addEventListener('click', () => {
    chrome.storage.sync.get((data) => {
      chrome.tabs.query({active: true, currentWindow: true},
          (tabs) => {
            chrome.tabs.sendMessage(tabs[0].id, {name: 'fill', data: data});
          });
    });
  });
});
