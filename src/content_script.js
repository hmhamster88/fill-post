chrome.runtime.onMessage.addListener(request => {
  if (request.name === 'fill') {
    fillextFill(request.data);
  }
});

function findLabelByText (text) {
  return $( 'label:contains(' + text + ')' )
}

function tableFromData(data) {
  return [
    {
      labelText: 'ФИО или наименование организации',
      text: data.fullName
    },
    {
      labelText: 'Адрес (населенный пункт, улица, дом, квартира)',
      text: data.address
    },
    {
      labelText: 'Индекс',
      text: data.index
    },
    {
      labelText: 'Серия',
      text: data.series
    },
    {
      labelText: 'Номер',
      text: data.number
    },
    {
      labelText: 'Когда выдан',
      text: data.when
    },
    {
      labelText: 'Кем выдан',
      text: data.who
    },
    {
      labelText: 'Адрес регистрации (не заполняется, если совпадает с адресом получателя)',
      text: data.addressReg
    }
  ];
}

function fillextFill(data) {
  var event = document.createEvent("HTMLEvents");
  const table = tableFromData(data);
  for (const row of table) {
    const label = findLabelByText(row.labelText);
    const input = label.parent().find('input');
    input.val(row.text);
    event.initEvent("input", true, true);
    input[0].dispatchEvent(event);
  }
}

